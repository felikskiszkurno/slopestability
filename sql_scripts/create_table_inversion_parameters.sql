CREATE TABLE InversionParameters (
    InversionID INTEGER PRIMARY KEY AUTOINCREMENT,
    Lambda NUMERIC,
    ParaDX NUMERIC,
    ParaDepthCoeff NUMERIC,
    Quality NUMERIC,
    Zpower NUMERIC,
    MaxDepthCoeff NUMERIC,
    WidthCoeff NUMERIC,
    MaxDepth NUMERIC
);