#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 01.04.2021

@author: Feliks Kiszkurno
"""

from .max_grad_classi import max_grad_classi
from .mgc_run import mgc_run