#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 19.01.2021

@author: Feliks Kiszkurno
"""

import slopestabilitytools
import random
import math
import settings


def split_dataset(test_names, random_seed, *, proportion=False):

    if proportion is False:
        proportion = settings.settings['split_proportion']

    random.seed(random_seed)

    test_number = len(test_names)
    test_prediction = random.sample(list(test_names),
                                     k=math.ceil(test_number * proportion))
    print('Number of tests used for prediction: '+str(len(test_prediction)))

    test_training = slopestabilitytools.set_diff(list(test_names), set(test_prediction))
    print('Number of tests used for training: ' + str(len(test_training)))

    return sorted(test_training), sorted(test_prediction)
