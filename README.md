# SlopeStability

** Author: Feliks Kiszkurno **

## Description:
Scripts and tools developed for the purpose of my Master Thesis

## Running
Before starting the main script, folder results/classifiers has to be manually create or emptied, if classifiers are not supposed to be resused

There cannot be any batch folders in data/prediction, that are empty

## Structure
### Main script
This is where most of the work happens.
### Modules
Functions and classes, that shouldn't be put in the main files. Assigned to different modules based on their purpose.
#### slopestabilitytools
All tools, that are not directly related to modeling, inversion, or classification and evaluation of the data.