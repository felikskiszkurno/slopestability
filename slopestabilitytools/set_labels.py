#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 16.01.2021

@author: Feliks Kiszkurno
"""

import matplotlib.pyplot as plt


def set_labels(axis_obj):

    axis_obj.set_xlabel('X [m]')
    axis_obj.set_ylabel('Depth [m]')

    return axis_obj
